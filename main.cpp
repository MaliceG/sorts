#include <QCoreApplication>
#include <QDebug>
#include <time.h>
#include <QElapsedTimer>

#include "sorts.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    srand(time(NULL));


//    const int elems = 10;
    QVector<int> array = {1,2,3,4,5,6,7,8,9,10};

//    for (int i = 0; i < elems; i++)
//    {
//        array.append(rand() % 20/*RAND_MAX*/);
//        qDebug() << array[i];
//    }

    Sorts::buildHeap(array, array.size() / 2 - 1, array.size());
    qDebug() << array;

//    Sorts::bubbleSort(array);
//    Sorts::insertionSort(array);
//    Sorts::selectionSort(array);
//    Sorts::shellSort(array);
//    Sorts::quickSort(array, 0, array.size()-1);
//    Sorts::mergeSort(array, 0, array.size()-1);
//    Sorts::combSort(array);
//    Sorts::heapSort(array);
//    Sorts::fastSort(array);


//    for (int i = 0; i < array.size(); i++)
//    {
//        qDebug() << "Sorted: " << array[i];
//    }

    return a.exec();
}
