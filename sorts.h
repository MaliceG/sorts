#ifndef SORTS_H
#define SORTS_H
#include <QDebug>


class Sorts
{
public:
    Sorts();
    static QVector<int> bubbleSort(QVector<int> array);
    static QVector<int> insertionSort(QVector<int> array);
    static QVector<int> selectionSort(QVector<int> array);
    static QVector<int> quickSort(QVector<int> array, int start, int end);
    static QVector<int> quick(QVector<int> &array, int start, int end);
    static QVector<int> shellSort(QVector<int> array);
    static int shellGetInterval(int index);
    static QVector<int> shellFindInterval(int size);
    static QVector<int> mergeSort(QVector<int> array, int start, int end);
    static void realMergeSort(QVector<int> &array, int start, int end);
    static void merge(QVector<int> &array, int start, int split, int end);
    static QVector<int> combSort(QVector<int> array);
    static QVector<int> heapSort(QVector<int> array);
    static void buildHeap(QVector<int> &array, int start, int end);
    static void chopHeap(QVector<int> &array);

private:
};

#endif // SORTS_H
