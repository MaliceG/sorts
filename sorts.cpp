#include "sorts.h"
#include <QElapsedTimer>
#include <array>

Sorts::Sorts()
{

}

QVector<int> Sorts::bubbleSort(QVector<int> array)
{
    //O(N^2)
    QElapsedTimer timer;
    timer.start();

    for (int i = 0 ; i < array.size(); i++)
    {
        for (int j = 1; j < array.size() - i; j++)
        {
            if (array[j-1] > array[j])
            {
                std::swap(array[j-1], array[j]);
            }
        }
    }

    qDebug() << "Bubble sort: " << timer.elapsed() << " ms";
    return array;

}

QVector<int> Sorts::insertionSort(QVector<int> array)
{
    //O(N^2)
    QElapsedTimer timer;
    timer.start();

    for (int i = 1; i < array.size(); i++)
        for (int j = i; j > 0 && array[j-1] > array[j]; j--)
        {
            std::swap(array[j-1], array[j]);
        }

    qDebug() << "Insertion sort: " << timer.elapsed() << " ms";
    return array;
}

QVector<int> Sorts::selectionSort(QVector<int> array)
{
    //O(N^2)
    QElapsedTimer timer;
    timer.start();

    for (int i = 0; i < array.size(); i++)
    {
        int minPos = i;
        for (int j = i + 1; j < array.size(); j++)
        {
            if (array[minPos] > array[j])
                minPos = j;
        }
        std::swap(array[i], array[minPos]);
    }

    qDebug() << "Selection sort: " << timer.elapsed() << " ms";
    return array;
}

QVector<int> Sorts::quick(QVector<int> &array, int start, int end)
{
    // выбираю опорный элемент
    int pivot = array[rand() % (end - start) + start];
    //между first & last всегда будет разница в 1, за исключением случае, когда оба итератора сделали шаг в разные стороны от опорного элемента
    //в таком случае опорный элемент можно далее не брать в учент, т.к. он уже стоит на своем упорядоченном месте
    int first = start;
    int last = end;

    //repeat until работает правильно
    do
    {
        while (array[first] < pivot) first++;
        while (array[last] > pivot) last--;

        if (first <= last)
        {
            std::swap(array[first], array[last]);
            first++;
            last--;
        }
    }
    while (first <= last);

    //если итератор последнего элемента не дошел до начального, значит между ними как минимум 2 элемента, которые можно отсортировать
    if (last > start) quick(array, start, last);

    //если разница между итератором левого элемента и последнего > 1, значит между ними есть 2+ элементов
    if (end - first > 0) quick(array, first, end);

    return array;
}

QVector<int> Sorts::quickSort(QVector<int> array, int start, int end)
{
    QElapsedTimer timer;
    timer.start();

    quick(array, start, end);

    qDebug() << "Quicksort: " << timer.elapsed() << " ms";
    return array;
}

QVector<int> Sorts::shellSort(QVector<int> array)
{
    //O(N^2)
    QElapsedTimer timer;
    timer.start();

    QVector<int> intervals = shellFindInterval(array.size());

    foreach (int interval, intervals)
    {
        //перебор каждого элемента, начиная с определенного интервала
        for (int i = interval; i < array.size(); i++)
        {
            //сдвигаемся от текущего элемента влево, до тех пор пока можем сделать шаг, равный интервалу
            //а также если элемент, стоящий слева на интервал шагов, больше текущего
            for (int j = i; j >= interval && array[j - interval] > array[j]; j -= interval)
                std::swap(array[j - interval], array[j]);
        }
    }

    qDebug() << "Shell sort: " << timer.elapsed() << " ms";
    return array;
}

int Sorts::shellGetInterval(int index)
{
    if (index % 2)
    {
        return (8 * 2 * index) - (6 * 2 * ((index + 1) / 2)) + 1;
    }
    else
    {
        return (9 * 2 * index) - (9 * 2 * (index / 2)) + 1;
    }

}

QVector<int> Sorts::shellFindInterval(int size)
{
    QVector<int> intervals = {1};
    int index = 1;
    while (shellGetInterval(index) < size / 3.f)
    {
        intervals.append(shellGetInterval(index));
        index++;
    }

    std::reverse(intervals.begin(), intervals.end());

    return intervals;
}

QVector<int> Sorts::mergeSort(QVector<int> array, int start, int end)
{
    QElapsedTimer timer;
    timer.start();

    realMergeSort(array, start, end);

    qDebug() << "Merge sort: " << timer.elapsed() << " ms";
    return array;
}

void Sorts::realMergeSort(QVector<int> &array, int start, int end)
{
    if (start < end)
    {
        int split = (start + end) / 2;
        realMergeSort(array, start, split);
        realMergeSort(array, split + 1, end);
        merge(array, start, split, end);
    }
    return;
}

void Sorts::merge(QVector<int> &array, int start, int split, int end)
{
    QVector<int> temp;
    int left = start;
    int right = split + 1;

    while (left <= split && right <= end)
    {
        if (array[left] < array[right])
            temp.append(array[left++]);
        else
            temp.append(array[right++]);
    }

    while (right <= end)
        temp.append(array[right++]);
    while (left <= split)
        temp.append(array[left++]);

    for (int i = 0; i < temp.count(); i++)
        array[start + i] = temp[i];
}

QVector<int> Sorts::combSort(QVector<int> array)
{
    QElapsedTimer timer;
    timer.start();

    const float fakt = 1.247; //фактор уменьшения
    int step = array.size() / fakt;
    while (step > 1)
    {
        for (int i = 0; i + step < array.size(); i++)
        {
            if (array[i] > array[i + step])
                std::swap(array[i], array[i + step]);
        }

        step /= fakt;
    }

    for (int i = 1; i < array.size(); i++)
    {
        for (int j = i; j < array.size(); j++)
            if (array[j-1] > array[j])
                std::swap(array[j-1], array[j]);
    }

    qDebug() << "Comb sort: " << timer.elapsed() << " ms";
    return array;
}

QVector<int> Sorts::heapSort(QVector<int> array)
{
    QElapsedTimer timer;
    timer.start();

    //начинаем с середины массива, т.к. там находятся узлы с потомками, далее идут сами потомки
    buildHeap(array, array.size() / 2 - 1, array.size());
    int end = array.size() - 1;
    while (end >= 0)
    {
        std::swap(array[0], array[end]);
        buildHeap(array, 0, end);
        end--;
    }

    qDebug() << "Heap sort: " << timer.elapsed() << " ms";
    return array;
}

void Sorts::buildHeap(QVector<int> &array, int start, int end)
{
    //строим кучу до последнего элемента end
    while (start >= 0)
    {
        int i = start;
        while (i < end)
        {
            //запоминаем позицию с наибольшим значением (на текущий момент сам узел)
            int biggestIndex = i;
            int leftChildPos = 2 * i + 1; //lpos 2i+1
            int rightChildPos = 2 * i + 2; //rpos 2i+2

            if (leftChildPos < end && array[biggestIndex] < array[leftChildPos])
                biggestIndex = leftChildPos;

            if (rightChildPos < end && array[biggestIndex] < array[rightChildPos])
                biggestIndex = rightChildPos;

            if (biggestIndex == i)
                break;

        /*
         * если изменился заменился узел в куче, тогда нужно перепроверить,
         * удовлетворяют ли его потомки необходимого условию (потомки меньше чем узел)
         */
            std::swap(array[i], array[biggestIndex]);
            i = biggestIndex;
        }

        start--;
    }
}
